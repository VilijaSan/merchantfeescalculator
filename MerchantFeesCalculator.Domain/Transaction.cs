﻿using System;

namespace MerchantFeesCalculator.Domain
{
    public class Transaction
    {
        public DateTime Date { get; }
        public string MerchantName { get; }
        public decimal Amount { get; }

        public Transaction(DateTime date, string merchantName, decimal amount)
        {
            Date = date;
            MerchantName = merchantName.ToUpperInvariant();
            Amount = amount;
        }
    }
}
