﻿namespace MerchantFeesCalculator.Domain
{
    public enum MerchantFeesCalculatorExecutionResult
    {
        Success = 0,
        Error = 1
    }
}
