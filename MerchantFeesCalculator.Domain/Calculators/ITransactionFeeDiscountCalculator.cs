﻿namespace MerchantFeesCalculator.Domain.Calculators
{
    public interface ITransactionFeeDiscountCalculator
    {
        decimal CalculateDiscount(decimal fee, Transaction transaction);
    }
}
