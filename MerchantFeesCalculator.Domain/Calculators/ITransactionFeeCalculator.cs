﻿namespace MerchantFeesCalculator.Domain.Calculators
{
    public interface ITransactionFeeCalculator
    {
        decimal CalculateFee(Transaction transaction);
    }
}
