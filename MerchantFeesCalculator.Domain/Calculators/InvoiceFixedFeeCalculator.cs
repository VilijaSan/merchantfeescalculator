﻿using System.Collections.Generic;

namespace MerchantFeesCalculator.Domain.Calculators
{
    public class InvoiceFixedFeeCalculator : IInvoiceFeeCalculator
    {
        private const decimal InvoiceFee = 29m;
        private static readonly Dictionary<string, string> MerchantLastTransactionMonth = new Dictionary<string, string>();

        public decimal CalculateInvoiceFee(decimal feeAfterDiscount, Transaction transaction)
        {
            var merchantName = transaction.MerchantName.ToUpperInvariant();
            var yearAndMonth = transaction.Date.ToString("yyyy-MM");

            if (!MerchantLastTransactionMonth.ContainsKey(merchantName))
            {
                MerchantLastTransactionMonth.Add(merchantName, yearAndMonth);
            }
            else if(MerchantLastTransactionMonth[merchantName].Equals(yearAndMonth))
            {
                return 0m;
            }
            else
            {
                MerchantLastTransactionMonth[merchantName] = yearAndMonth;                
            }

            return feeAfterDiscount == 0m ? 0m : InvoiceFee;
        }
    }
}
