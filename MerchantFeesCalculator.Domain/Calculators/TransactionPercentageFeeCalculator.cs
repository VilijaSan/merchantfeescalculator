﻿namespace MerchantFeesCalculator.Domain.Calculators
{
    public class TransactionPercentageFeeCalculator : ITransactionFeeCalculator
    {
        private const decimal TransactionFeePercent = 1m;

        public decimal CalculateFee(Transaction transaction)
        {
            return transaction.Amount * TransactionFeePercent / 100m;
        }
    }
}
