﻿namespace MerchantFeesCalculator.Domain.Calculators
{
    public interface ITotalMerchantTransactionFeeCalculator
    {
        decimal CalculateFee(Transaction transaction);
    }
}
