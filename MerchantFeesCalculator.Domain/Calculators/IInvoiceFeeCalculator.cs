﻿namespace MerchantFeesCalculator.Domain.Calculators
{
    public interface IInvoiceFeeCalculator
    {
        decimal CalculateInvoiceFee(decimal feeAfterDiscount, Transaction transaction);
    }
}
