﻿namespace MerchantFeesCalculator.Domain.Calculators
{
    public class TotalMerchantTransactionFeeCalculator : ITotalMerchantTransactionFeeCalculator
    {
        private readonly ITransactionFeeCalculator _feeCalculator;
        private readonly ITransactionFeeDiscountCalculator _feeDiscountCalculator;
        private readonly IInvoiceFeeCalculator _invoiceFeeCalculator;

        public TotalMerchantTransactionFeeCalculator(
            ITransactionFeeCalculator feeCalculator, 
            ITransactionFeeDiscountCalculator feeDiscountCalculator, 
            IInvoiceFeeCalculator invoiceFeeCalculator)
        {
            _feeCalculator = feeCalculator;
            _feeDiscountCalculator = feeDiscountCalculator;
            _invoiceFeeCalculator = invoiceFeeCalculator;
        }
     
        public decimal CalculateFee(Transaction transaction)
        {
            var transactionFee = _feeCalculator.CalculateFee(transaction);
            var feeDiscount = _feeDiscountCalculator.CalculateDiscount(transactionFee, transaction);
            var feeAfterDiscount = transactionFee - feeDiscount;
            var invoiceFee = _invoiceFeeCalculator.CalculateInvoiceFee(feeAfterDiscount, transaction);

            return feeAfterDiscount + invoiceFee;
        }
    }
}
