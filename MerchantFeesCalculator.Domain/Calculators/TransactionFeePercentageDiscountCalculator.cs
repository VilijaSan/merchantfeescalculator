﻿using System.Collections.Generic;

namespace MerchantFeesCalculator.Domain.Calculators
{
    public class TransactionFeePercentageDiscountCalculator : ITransactionFeeDiscountCalculator
    {
        private static readonly Dictionary<string, decimal> MerchantDiscounts = new Dictionary<string, decimal>
        {
            { "TELIA", 10m },
            { "CIRCLE_K", 20m }
        };

        public decimal CalculateDiscount(decimal fee, Transaction transaction)
        {
            var merchantName = transaction.MerchantName.ToUpperInvariant();

            if (!MerchantDiscounts.ContainsKey(merchantName))
            {
                return 0;
            }

            return fee * MerchantDiscounts[merchantName] / 100m;
        }
    }
}
