﻿namespace MerchantFeesCalculator.Domain.Services
{
    public interface IInputReader
    {
        void ReadLine();
    }
}
