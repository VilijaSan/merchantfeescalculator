﻿using System.Collections.Generic;

namespace MerchantFeesCalculator.Domain.Services
{
    public interface ITransactionsReader
    {
        IEnumerable<Transaction> ReadTransactions();
    }
}
