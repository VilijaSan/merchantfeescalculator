﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace MerchantFeesCalculator.Domain.Services
{
    public class TransactionsFileReader : ITransactionsReader
    {
        private readonly string _transactionFilePath;

        private readonly IOutputWriter _outputWriter;

        public TransactionsFileReader(IOutputWriter outputWriter, string transactionFilePath)
        {
            _outputWriter = outputWriter;
            _transactionFilePath = transactionFilePath;
        }

        public IEnumerable<Transaction> ReadTransactions()
        {
            return File.ReadLines(_transactionFilePath)
                .Select(ParseTransaction)
                .Where(x => x != null);
        }

        private Transaction ParseTransaction(string transaction)
        {
            var transactionData = transaction.Split(" ", StringSplitOptions.RemoveEmptyEntries);

            if (!transactionData.Any())
            {
                _outputWriter.WriteLine(string.Empty);
                return null;
            }

            if (transactionData.Length != 3)
            {
                _outputWriter.WriteLine($"Incorrect transaction format {string.Join(" ", transactionData)}. " +
                                       "The correct format is: date merchantName amount");
                return null;
            }

            var dateString = transactionData[0];

            var date = GetDate(dateString);
            if (date == null)
            {
                _outputWriter.WriteLine($"Could not parse the date from {dateString}");
                return null;
            }

            var merchant = transactionData[1];

            var amount = GetAmount(transactionData[2]);
            if (amount == null || amount < 0)
            {
                _outputWriter.WriteLine($"Could not parse the transaction amount from {transactionData[2]}");
                return null;
            }

            return new Transaction(date.Value, merchant, amount.Value);
        }

        private static decimal? GetAmount(string amount)
        {
            try
            {
                return decimal.Parse(amount, new NumberFormatInfo { NumberDecimalSeparator = "." });
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static DateTime? GetDate(string date)
        {
            try
            {
                return DateTime.Parse(date);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
