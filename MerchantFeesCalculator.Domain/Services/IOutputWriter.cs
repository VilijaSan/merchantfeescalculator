﻿namespace MerchantFeesCalculator.Domain.Services
{
    public interface IOutputWriter
    {
        void WriteLine(string text);
    }
}
