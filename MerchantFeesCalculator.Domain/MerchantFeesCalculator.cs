﻿using System;
using System.Globalization;
using MerchantFeesCalculator.Domain.Calculators;
using MerchantFeesCalculator.Domain.Services;

namespace MerchantFeesCalculator.Domain
{
    public class MerchantFeesCalculator
    {
        private readonly IOutputWriter _outputWriter;
        private readonly IInputReader _inputReader;
        private readonly ITransactionsReader _transactionsReader;
        private readonly ITotalMerchantTransactionFeeCalculator _totalMerchantTransactionFeeCalculator;

        public MerchantFeesCalculator(
            IOutputWriter outputWriter,
            ITransactionsReader transactionsReader,
            IInputReader inputReader,
            ITotalMerchantTransactionFeeCalculator totalMerchantTransactionFeeCalculator)
        {
            _outputWriter = outputWriter;
            _transactionsReader = transactionsReader;
            _inputReader = inputReader;
            _totalMerchantTransactionFeeCalculator = totalMerchantTransactionFeeCalculator;
        }

        public MerchantFeesCalculatorExecutionResult Execute()
        {
            try
            {
                var transactions = _transactionsReader.ReadTransactions();

                foreach (var transaction in transactions)
                {
                    var totalFee = _totalMerchantTransactionFeeCalculator.CalculateFee(transaction);
                    OutputTransactionFee(transaction, totalFee);
                }
            }
            catch (Exception ex)
            {
                _outputWriter.WriteLine(ex.Message);
                _inputReader.ReadLine();

                return MerchantFeesCalculatorExecutionResult.Error;
            }

            _inputReader.ReadLine();
            return MerchantFeesCalculatorExecutionResult.Success;
        }

        private void OutputTransactionFee(Transaction transaction, decimal totalFee)
        {
            _outputWriter.WriteLine($"{transaction.Date:yyyy-MM-dd} " +
                                    $"{transaction.MerchantName} " +
                                    $"{totalFee.ToString("F", new NumberFormatInfo { NumberDecimalSeparator = "." })}");
        }
    }
}
