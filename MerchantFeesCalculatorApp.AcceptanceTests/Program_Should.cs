﻿using System.Diagnostics;
using System.IO;
using Xunit;

namespace MerchantFeesCalculatorApp.AcceptanceTests
{
    public class Program_Should
    {
        [Fact]
        public void Output_Correctly_Calculated_Merchant_Transaction_Fees_In_The_Correct_Format()
        {
            var myProcess = new Process
            {
                StartInfo =
                {
                    RedirectStandardOutput = true, 
                    RedirectStandardInput = true,
                    FileName = "..\\..\\..\\..\\MerchantFeesCalculationApp\\bin\\Debug\\netcoreapp3.0\\MerchantFeesCalculationApp.exe"
                }
            };

            myProcess.Start();

            myProcess.StandardInput.WriteLine();
            myProcess.WaitForExit();

            var result = myProcess.ExitCode;

            var output = myProcess.StandardOutput.ReadToEnd().TrimEnd();
            var expectedOutput = File.ReadAllText("expectedoutput.txt");

            Assert.True(result == 0);
            Assert.Equal(expectedOutput, output);
        }
    }
}
