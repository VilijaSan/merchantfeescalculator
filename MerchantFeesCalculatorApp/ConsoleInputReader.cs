﻿using System;
using MerchantFeesCalculator.Domain.Services;

namespace MerchantFeesCalculatorApp
{
    public class ConsoleInputReader : IInputReader
    {
        public void ReadLine()
        {
            Console.ReadLine();
        }
    }
}
