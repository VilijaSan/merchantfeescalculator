﻿using System;
using MerchantFeesCalculator.Domain.Services;

namespace MerchantFeesCalculatorApp
{
    public class ConsoleOutputWriter : IOutputWriter
    {
        public void WriteLine(string text)
        {
            Console.WriteLine(text);
        }
    }
}
