﻿using MerchantFeesCalculator.Domain.Calculators;
using MerchantFeesCalculator.Domain.Services;

namespace MerchantFeesCalculatorApp
{
    public class Program
    {
        private static readonly IOutputWriter OutputWriter = new ConsoleOutputWriter();
        private static readonly IInputReader InputReader = new ConsoleInputReader();

        private static readonly ITotalMerchantTransactionFeeCalculator TotalMerchantTransactionFeeCalculator =
                new TotalMerchantTransactionFeeCalculator(
                    new TransactionPercentageFeeCalculator(),
                    new TransactionFeePercentageDiscountCalculator(),
                    new InvoiceFixedFeeCalculator());

        private static readonly MerchantFeesCalculator.Domain.MerchantFeesCalculator MerchantFeesCalculationApp = 
            new MerchantFeesCalculator.Domain.MerchantFeesCalculator(
                OutputWriter,
                new TransactionsFileReader(OutputWriter, "transactions.txt"), 
                InputReader,
                TotalMerchantTransactionFeeCalculator);

        public static int Main()
        {
            return (int) MerchantFeesCalculationApp.Execute();
        }
    }
}
