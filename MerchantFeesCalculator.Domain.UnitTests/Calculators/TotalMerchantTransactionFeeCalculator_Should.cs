﻿using System;
using MerchantFeesCalculator.Domain.Calculators;
using Moq;
using Xunit;

namespace MerchantFeesCalculator.Domain.UnitTests.Calculators
{
    public class TotalMerchantTransactionFeeCalculator_Should
    {
        [Theory, AutoMoqData]
        public void Calculate_Fee_Subtract_Discount_Add_Invoice_Fee_And_Return_Total_Fee(
            Mock<ITransactionFeeCalculator> feeCalculatorMock,
            Mock<ITransactionFeeDiscountCalculator> feeDiscountCalculatorMock,
            Mock<IInvoiceFeeCalculator> invoiceFeeCalculatorMock)
        {
            // Arrange
            var sut = new TotalMerchantTransactionFeeCalculator(
                feeCalculatorMock.Object, 
                feeDiscountCalculatorMock.Object, 
                invoiceFeeCalculatorMock.Object);

            var transaction = new Transaction(new DateTime(), "Merchant", 100m);

            feeCalculatorMock.Setup(x => x.CalculateFee(transaction)).Returns(1m);
            feeDiscountCalculatorMock.Setup(x => x.CalculateDiscount(1m, transaction)).Returns(0m);
            invoiceFeeCalculatorMock.Setup(x => x.CalculateInvoiceFee(1m, transaction)).Returns(29m);

            // Act
            var result = sut.CalculateFee(transaction);

            // Assert
            Assert.Equal(30, result);

            feeCalculatorMock.Verify(x => x.CalculateFee(transaction), Times.Once);

            feeDiscountCalculatorMock.Verify(x => x.CalculateDiscount(1m, transaction), Times.Once);

            invoiceFeeCalculatorMock.Verify(x => x.CalculateInvoiceFee(1m, transaction), Times.Once);
        }
    }
}
