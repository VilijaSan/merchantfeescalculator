﻿using System;
using AutoFixture.Xunit2;
using MerchantFeesCalculator.Domain.Calculators;
using Xunit;

namespace MerchantFeesCalculator.Domain.UnitTests.Calculators
{
    public class InvoiceFeeCalculator_Should
    {
        private const decimal FixedInvoiceFee = 29m;

        [Theory, AutoData]
        public void Return_Fixed_Invoice_Fee_If_It_Is_The_First_Transaction_For_The_Merchant_And_Fee_After_Discount_Is_Not_Zero(
            Transaction transaction,
            InvoiceFixedFeeCalculator sut)
        {
            // Arrange
            // Act
            var actualInvoiceFee = sut.CalculateInvoiceFee(10m, transaction);

            //Assert
            Assert.Equal(FixedInvoiceFee, actualInvoiceFee);
        }

        [Theory, AutoData]
        public void Return_Zero_If_It_Is_The_First_Transaction_For_The_Merchant_And_Fee_After_Discount_Is_Zero(
            Transaction transaction,
            InvoiceFixedFeeCalculator sut)
        {
            // Arrange
            // Act
            var actualInvoiceFee = sut.CalculateInvoiceFee(0m, transaction);

            //Assert
            Assert.Equal(0, actualInvoiceFee);
        }

        [Theory, AutoData]
        public void Return_Fixed_Invoice_Fee_If_It_Is_The_First_Merchant_Transaction_That_Month_And_Fee_After_Discount_Is_Not_Zero(
            string merchantName,
            decimal transactionAmount,
            InvoiceFixedFeeCalculator sut)
        {
            // Arrange
            var firstTransaction = new Transaction(new DateTime(2019, 01, 01), merchantName, transactionAmount);
            var secondTransaction = new Transaction(new DateTime(2019, 02, 01), merchantName, transactionAmount);

            // Act
            var firstInvoiceFee = sut.CalculateInvoiceFee(10m, firstTransaction);
            var secondInvoiceFee = sut.CalculateInvoiceFee(10m, secondTransaction);

            //Assert
            Assert.Equal(FixedInvoiceFee, secondInvoiceFee);
        }

        [Theory, AutoData]
        public void Return_Zero_If_It_Is_The_First_Merchant_Transaction_That_Month_And_Fee_After_Discount_Is_Zero(
            string merchantName,
            decimal transactionAmount, 
            InvoiceFixedFeeCalculator sut)
        {
            // Arrange
            var firstTransaction = new Transaction(new DateTime(2019, 01, 01), merchantName, transactionAmount);
            var secondTransaction = new Transaction(new DateTime(2019, 02, 01), merchantName, transactionAmount);

            // Act
            var firstInvoiceFee = sut.CalculateInvoiceFee(10m, firstTransaction);
            var secondInvoiceFee = sut.CalculateInvoiceFee(0m, secondTransaction);

            //Assert
            Assert.Equal(0, secondInvoiceFee);
        }

        [Theory, AutoData]
        public void Return_Zero_If_A_Merchant_Has_Already_Made_Another_Transaction_That_Month_And_Fee_After_Discount_Is_Not_Zero(
            string merchantName,
            decimal transactionAmount,
            InvoiceFixedFeeCalculator sut)
        {
            // Arrange
            var firstTransaction = new Transaction(new DateTime(2019, 01, 01), merchantName, transactionAmount);
            var secondTransaction = new Transaction(new DateTime(2019, 01, 02), merchantName, transactionAmount);

            // Act
            var firstInvoiceFee = sut.CalculateInvoiceFee(1m, firstTransaction);
            var secondInvoiceFee = sut.CalculateInvoiceFee(1m, secondTransaction);

            //Assert
            Assert.Equal(0m, secondInvoiceFee);
        }

        [Theory, AutoData]
        public void Return_Zero_If_A_Merchant_Has_Already_Made_Another_Transaction_That_Month_And_Fee_After_Discount_Is_Zero(
            string merchantName,
            decimal transactionAmount,
            InvoiceFixedFeeCalculator sut)
        {
            // Arrange
            var firstTransaction = new Transaction(new DateTime(2019, 01, 01), merchantName, transactionAmount);
            var secondTransaction = new Transaction(new DateTime(2019, 01, 02), merchantName, transactionAmount);

            // Act
            var firstInvoiceFee = sut.CalculateInvoiceFee(1m, firstTransaction);
            var secondInvoiceFee = sut.CalculateInvoiceFee(0m, secondTransaction);

            //Assert
            Assert.Equal(0m, secondInvoiceFee);
        }
    }
}
