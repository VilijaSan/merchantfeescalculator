﻿using System;
using System.Collections.Generic;
using AutoFixture.Xunit2;
using MerchantFeesCalculator.Domain.Calculators;
using Xunit;

namespace MerchantFeesCalculator.Domain.UnitTests.Calculators
{
    public class TransactionFeeCalculator_Should
    {
        [Theory]
        [MemberAutoData(nameof(TransactionData))]
        public void Apply_One_Percent_Fee_To_Transaction(
            Transaction transaction, 
            decimal expectedFee, 
            TransactionPercentageFeeCalculator sut)
        {
            // Arrange
            // Act
            var actualFee = sut.CalculateFee(transaction);

            //Assert
            Assert.Equal(expectedFee, actualFee);
        }

        public static IEnumerable<object[]> TransactionData =>
            new List<object[]>
            {
                new object[] { new Transaction(new DateTime(), "merchant", 0m), 0m },
                new object[] { new Transaction(new DateTime(), "merchant", 100m), 1m },
                new object[] { new Transaction(new DateTime(), "merchant", 120.5m), 1.205m },
                new object[] { new Transaction(new DateTime(), "merchant", decimal.MaxValue), decimal.MaxValue * 0.01m }
            };
    }
}
