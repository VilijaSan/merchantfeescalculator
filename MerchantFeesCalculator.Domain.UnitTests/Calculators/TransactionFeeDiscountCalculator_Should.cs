﻿using System;
using System.Collections.Generic;
using AutoFixture.Xunit2;
using MerchantFeesCalculator.Domain.Calculators;
using Xunit;

namespace MerchantFeesCalculator.Domain.UnitTests.Calculators
{
    public class TransactionFeeDiscountCalculator_Should
    {
        [Theory]
        [MemberAutoData(nameof(MerchantWithoutDiscountData))]
        public void Return_Zero_If_The_Merchant_Does_Not_Have_A_Discount(
            decimal fee, 
            Transaction transaction, 
            TransactionFeePercentageDiscountCalculator sut)
        {
            // Arrange
            // Act
            var actualDiscount = sut.CalculateDiscount(fee, transaction);

            //Assert
            Assert.Equal(0, actualDiscount);
        }

        [Theory]
        [MemberAutoData(nameof(TeliaDiscountData))]
        public void Return_Ten_Percent_Fee_Discount_For_Telia(
            decimal fee, 
            decimal expectedDiscount, 
            Transaction transaction, 
            TransactionFeePercentageDiscountCalculator sut)
        {
            // Arrange
            // Act
            var actualDiscount = sut.CalculateDiscount(fee, transaction);

            //Assert
            Assert.Equal(expectedDiscount, actualDiscount);
        }

        [Theory]
        [MemberAutoData(nameof(CircleKDiscountData))]
        public void Return_Twenty_Percent_Fee_Discount_For_Circle_K(
            decimal fee, 
            decimal expectedDiscount, 
            Transaction transaction, 
            TransactionFeePercentageDiscountCalculator sut)
        {
            // Arrange
            // Act
            var actualDiscount = sut.CalculateDiscount(fee, transaction);

            //Assert
            Assert.Equal(expectedDiscount, actualDiscount);
        }

        public static IEnumerable<object[]> MerchantWithoutDiscountData =>
            new List<object[]>
            {
                new object[] { 1m, new Transaction(new DateTime(), "MerchantWithoutDiscount", 100m) },
                new object[] { 0.1m, new Transaction(new DateTime(), "TEKIA", 10m) },
                new object[] { 1m, new Transaction(new DateTime(), "", 100m) }
            };

        public static IEnumerable<object[]> TeliaDiscountData =>
            new List<object[]>
            {
                new object[] { 1m, 0.1m, new Transaction(new DateTime(), "telia", 100m) },
                new object[] { 25m, 2.5m, new Transaction(new DateTime(), "TELIA", 2500m) }
            };

        public static IEnumerable<object[]> CircleKDiscountData =>
            new List<object[]>
            {
                new object[] { 100m, 20m, new Transaction(new DateTime(), "circle_k", 10000m) },
                new object[] { 15m, 3m, new Transaction(new DateTime(), "CIRCLE_K", 1500m) }
            };
    }
}
