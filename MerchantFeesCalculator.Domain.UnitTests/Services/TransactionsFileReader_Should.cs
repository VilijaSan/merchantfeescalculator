﻿using System.Linq;
using MerchantFeesCalculator.Domain.Services;
using Moq;
using Xunit;

namespace MerchantFeesCalculator.Domain.UnitTests.Services
{
    public class TransactionsFileReader_Should
    {
        [Theory, AutoMoqData]
        public void Return_Transactions_With_Values_From_File(Mock<IOutputWriter> outputWriterMock)
        {
            // Arrange
            var sut = new TransactionsFileReader(outputWriterMock.Object, "testtransactionssmaller.txt");

            // Act
            var result = sut.ReadTransactions().ToList();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count);

            Assert.Equal("2018-09-01", result[0].Date.ToString("yyyy-MM-dd"));
            Assert.Equal("7-ELEVEN", result[0].MerchantName);
            Assert.Equal(100m, result[0].Amount);

            Assert.Equal("2018-09-04", result[1].Date.ToString("yyyy-MM-dd"));
            Assert.Equal("CIRCLE_K", result[1].MerchantName);
            Assert.Equal(100.5m, result[1].Amount);
        }

        [Theory, AutoMoqData]
        public void Output_Empty_Line_And_Return_No_Transaction_For_Empty_Lines(
            Mock<IOutputWriter> outputWriterMock)
        {
            // Arrange
            var sut = new TransactionsFileReader(outputWriterMock.Object, "testtransactionsempty.txt");

            // Act
            var result = sut.ReadTransactions();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count());
            outputWriterMock.Verify(x => x.WriteLine(string.Empty), Times.Exactly(2));
        }

        [Theory, AutoMoqData]
        public void Output_Message_And_Not_Return_Transaction_If_The_Transaction_Format_In_File_Is_Incorrect(
            Mock<IOutputWriter> outputWriterMock)
        {
            // Arrange
            var sut = new TransactionsFileReader(outputWriterMock.Object, "testtransactionsbadformat.txt");

            // Act
            var result = sut.ReadTransactions();

            // Assert
            Assert.Empty(result);
            outputWriterMock.Verify(x => 
                    x.WriteLine("Incorrect transaction format MERCHANT 100. "
                                + "The correct format is: date merchantName amount"), 
                Times.Once);
        }

        [Theory, AutoMoqData]
        public void Output_Message_And_Not_Return_Transaction_If_The_Date_Format_In_File_Is_Incorrect(
            Mock<IOutputWriter> outputWriterMock)
        {
            // Arrange
            var sut = new TransactionsFileReader(outputWriterMock.Object, "testtransactionsbaddate.txt");

            // Act
            var result = sut.ReadTransactions();

            // Assert
            Assert.Empty(result);
            outputWriterMock.Verify(x => 
                x.WriteLine("Could not parse the date from 2019-01-100"), 
                Times.Once);
        }

        [Theory, AutoMoqData]
        public void Output_Message_And_Not_Return_Transaction_If_The_Amount_Format_In_File_Is_Incorrect(
            Mock<IOutputWriter> outputWriterMock)
        {
            // Arrange
            var sut = new TransactionsFileReader(outputWriterMock.Object, "testtransactionsbadamount.txt");

            // Act
            var result = sut.ReadTransactions();

            // Assert
            Assert.Empty(result);
            outputWriterMock.Verify(x => 
                x.WriteLine("Could not parse the transaction amount from hundred"), 
                Times.Once);
        }

        [Theory, AutoMoqData]
        public void Output_Message_And_Not_Return_Transaction_If_The_Amount_In_File_Is_Negative(
            Mock<IOutputWriter> outputWriterMock)
        {
            // Arrange
            var sut = new TransactionsFileReader(outputWriterMock.Object, "testtransactionsnegativeamount.txt");

            // Act
            var result = sut.ReadTransactions();

            // Assert
            Assert.Empty(result);
            outputWriterMock.Verify(x => 
                x.WriteLine("Could not parse the transaction amount from -100"), 
                Times.Once);
        }
    }
}
