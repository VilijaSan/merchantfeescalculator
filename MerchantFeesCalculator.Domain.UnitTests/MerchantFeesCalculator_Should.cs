﻿using System;
using MerchantFeesCalculator.Domain.Calculators;
using MerchantFeesCalculator.Domain.Services;
using Moq;
using Xunit;

namespace MerchantFeesCalculator.Domain.UnitTests
{
    public class MerchantFeesCalculator_Should
    {
        [Theory, AutoMoqData]
        public void Execute_Successfully_And_Return_Success_Result(
            Mock<IOutputWriter> outputWriterMock, 
            Mock<IInputReader> inputReaderMock, 
            Mock<ITransactionsReader> transactionReaderMock,
            Mock<ITotalMerchantTransactionFeeCalculator> totalMerchantFeesCalculator)
        {
            // Arrange
            var sut = new global::MerchantFeesCalculator.Domain.MerchantFeesCalculator(
                outputWriterMock.Object,
                transactionReaderMock.Object,
                inputReaderMock.Object,
                totalMerchantFeesCalculator.Object);

            // Act
            var result = sut.Execute();

            // Assert
            Assert.Equal(MerchantFeesCalculatorExecutionResult.Success, result);
        }

        [Theory, AutoMoqData]
        public void Return_Error_Result_And_Output_Exception_Message_If_Exception_Occurs(
            Mock<IOutputWriter> outputWriterMock,
            Mock<IInputReader> inputReaderMock,
            Mock<ITransactionsReader> transactionReaderMock,
            Mock<ITotalMerchantTransactionFeeCalculator> totalMerchantFeesCalculator,
            Exception exception)
        {
            // Arrange
            transactionReaderMock.Setup(x => x.ReadTransactions()).Throws(exception);

            var sut = new global::MerchantFeesCalculator.Domain.MerchantFeesCalculator(
                outputWriterMock.Object,
                transactionReaderMock.Object,
                inputReaderMock.Object,
                totalMerchantFeesCalculator.Object);

            // Act
            var result = sut.Execute();

            // Assert
            Assert.Equal(MerchantFeesCalculatorExecutionResult.Error, result);
            outputWriterMock.Verify(x => x.WriteLine(exception.Message));
        }
    }
}
